 const createNewUser = () => {
    let firstName = prompt("Enter your first name");
    let lastName = prompt("Enter your last name");
    let birthdayStr = prompt("Enter your birthdate dd.mm.yyyy:");
    let [day, month, year] = birthdayStr.split('.');
    let birthday = new Date(`${year}-${month}-${day}`);

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const currentDate = new Date();
            const birthDate = new Date(this.birthday);
            let age = currentDate.getFullYear() - birthDate.getFullYear();
            const monthDiff = currentDate.getMonth() - birthDate.getMonth();
            if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword() {
            const year = new Date(this.birthday).getFullYear();
            const firstLetter = this.firstName.charAt(0).toUpperCase();
            const lastNameLower = this.lastName.toLowerCase();
            return firstLetter + lastNameLower + year;
        }
    };
    return newUser;
};
let user = createNewUser();
let login = user.getLogin();
console.log(login);
let age = user.getAge();
console.log(age);
let password = user.getPassword();
console.log(password);

